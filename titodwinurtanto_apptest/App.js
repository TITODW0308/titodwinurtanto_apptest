import React, { Component } from 'react';

import { StyleSheet, View, Alert, TextInput, Button, Text, Platform, TouchableOpacity, ActivityIndicator } from 'react-native';

import { ListView } from 'deprecated-react-native-listview';

import { createStackNavigator } from 'react-navigation-stack';

import {
	createAppContainer, // ini yg di export
} from 'react-navigation';

import { store } from './store';
import { Provider } from 'react-redux';

class MainActivity extends Component {

  static navigationOptions =
  {
     title: 'MainActivity',
  };

constructor(props) {

   super(props)

   this.state = {

     TextInput_Nasabah_Name: '',
     TextInput_Nasabah_Address: '',
     TextInput_Nasabah_PhoneNumber: '',
     TextInput_Nasabah_Email: '',

   }

 }

 InsertNasabahRecordsToServer = () =>{

      fetch('http://appldev.com/nasabah/InsertNasabahData.php', {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({

        name : this.state.TextInput_Nasabah_Name,

        address : this.state.TextInput_Nasabah_Address,

        phone : this.state.TextInput_Nasabah_PhoneNumber,

        email: this.state.TextInput_Nasabah_Email

      })

      }).then((response) => response.json())
          .then((responseJson) => {

            // Showing response message coming from server after inserting records.
            Alert.alert(responseJson);

          }).catch((error) => {
            console.error(error);
          });

}

 GoTo_Show_NasabahList_Activity_Function = () =>
  {
    this.props.navigation.navigate('Second');
    
  }

 render() {
   return (

<View style={styles.MainContainer}>


       <Text style={{fontSize: 20, textAlign: 'center', marginBottom: 7}}> Nasabah Registration Form </Text>
 
       <TextInput
         
         placeholder="Enter Nasabah Name"

         onChangeText={ TextInputValue => this.setState({ TextInput_Nasabah_Name : TextInputValue }) }

         underlineColorAndroid='transparent'

         style={styles.TextInputStyleClass}
       />

      <TextInput
         
         placeholder="Enter Nasabah Address"

         onChangeText={ TextInputValue => this.setState({ TextInput_Nasabah_Address : TextInputValue }) }

         underlineColorAndroid='transparent'

         style={styles.TextInputStyleClass}
       />

      <TextInput
         
         placeholder="Enter Nasabah Phone Number"

         onChangeText={ TextInputValue => this.setState({ TextInput_Nasabah_PhoneNumber : TextInputValue }) }

         underlineColorAndroid='transparent'

         style={styles.TextInputStyleClass}
       />

       <TextInput

         placeholder="Enter Nasabah Email"

         onChangeText={ TextInputValue => this.setState({ TextInput_Nasabah_Email : TextInputValue }) }

         underlineColorAndroid='transparent'

         style={styles.TextInputStyleClass}
       />

      <TouchableOpacity activeOpacity = { .4 } style={styles.TouchableOpacityStyle} onPress={this.InsertNasabahRecordsToServer} >

        <Text style={styles.TextStyle}> INSERT NASABAH RECORD TO SERVER </Text>

      </TouchableOpacity>

      <TouchableOpacity activeOpacity = { .4 } style={styles.TouchableOpacityStyle} onPress={this.GoTo_Show_NasabahList_Activity_Function} >

        <Text style={styles.TextStyle}> SHOW ALL INSERTED NASABAH RECORDS IN LISTVIEW </Text>

      </TouchableOpacity>
 

</View>
           
   );
 }
}

class ShowNasabahListActivity extends Component {

  constructor(props) { 

    super(props);

    this.state = {

      isLoading: true,
      dataSource: null

    }
  }

  static navigationOptions =
  {
     title: 'ShowNasabahListActivity',
  };

  componentDidMount() {
    
       return fetch('http://appldev.com/nasabah/ShowAllNasabahList.php')
         .then((response) => response.json())
         .then((responseJson) => {
          // this.setState({
          //   dataSource: new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2})
          // });
          this.setState({
             isLoading: false,
            //  dataSource: this.state.dataSource.cloneWithRows(responseJson),
             dataSource: responseJson,
           }, function() {
             // In this block you can do something with new state.
           });
         })
         .catch((error) => {
           console.error(error);
         });
     }
    
     GetNasabahIDFunction=(id,name, address, phone, email)=>{

          this.props.navigation.navigate('Third', { 

            ID : id,
            NAME : name,
            ADDRESS : address,
            PHONE_NUMBER : phone,
            EMAIL : email

          });

     }

     ListViewItemSeparator = () => {
       return (
         <View
           style={{
             height: .5,
             width: "100%",
             backgroundColor: "#000",
           }}
         />
       );
     }

     render() {
      if (this.state.isLoading) {
        return (
          <View style={{flex: 1, paddingTop: 20}}>
            <ActivityIndicator />
          </View>
        );
      }
   
      return (
   
        <View style={styles.MainContainer_For_Show_NasabahList_Activity}>
   
          {/* <ListView
   
            dataSource={this.state.dataSource}
   
            renderSeparator= {this.ListViewItemSeparator}
   
            renderRow={ (rowData) => <Text style={styles.rowViewContainer} 

                      onPress={this.GetNasabahIDFunction.bind(
                        this, rowData.id,
                         rowData.name, 
                         rowData.address, 
                         rowData.phone, 
                         rowData.email
                         )} > 

                      {rowData.name} 
                      
                      </Text> }
   
          /> */}
          { this.state.dataSource.map((value, index) => {
            return(<Text style={styles.rowViewContainer} 
              onPress={this.GetNasabahIDFunction.bind(
                this, value.id,
                value.name, 
                value.address, 
                value.phone, 
                value.email
                 )}
              key={index}>{value.name}</Text>)
          }) }
   
        </View>
      );
    }

}

class EditNasabahRecordActivity extends Component {
  
  constructor(props) {
    
       super(props)
    
       this.state = {
    
         TextInput_Nasabah_ID: '',
         TextInput_Nasabah_Name: '',
         TextInput_Nasabah_Address: '',
         TextInput_Nasabah_PhoneNumber: '',
         TextInput_Nasabah_Email: '',
    
       }
    
     }

     componentDidMount(){

      // Received Nasabah Details Sent From Previous Activity and Set Into State.
      this.setState({ 
        TextInput_Nasabah_ID : this.props.navigation.state.params.ID,
        TextInput_Nasabah_Name: this.props.navigation.state.params.NAME,
        TextInput_Nasabah_Address: this.props.navigation.state.params.ADDRESS,
        TextInput_Nasabah_PhoneNumber: this.props.navigation.state.params.PHONE_NUMBER,
        TextInput_Nasabah_Email: this.props.navigation.state.params.EMAIL,
      })

     }
  
    static navigationOptions =
    {
       title: 'EditNasabahRecordActivity',
    };

    UpdateNasabahRecord = () =>{
      
            fetch('http://appldev.com/nasabah/UpdateNasabahRecord.php', {
            method: 'POST',
            headers: {
              'Accept': 'application/json',
              'Content-Type': 'application/json',
            },
            body: JSON.stringify({
      
              id : this.state.TextInput_Nasabah_ID,

              name : this.state.TextInput_Nasabah_Name,
      
              address : this.state.TextInput_Nasabah_Address,
      
              phone : this.state.TextInput_Nasabah_PhoneNumber,
      
              email: this.state.TextInput_Nasabah_Email
      
            })
      
            }).then((response) => response.json())
                .then((responseJson) => {
      
                  // Showing response message coming from server updating records.
                  Alert.alert(responseJson);
      
                }).catch((error) => {
                  console.error(error);
                });
      
      }


    DeleteNasabahRecord = () =>{
        
          fetch('http://appldev.com/nasabah/DeleteNasabahRecord.php', {
          method: 'POST',
          headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          },
          body: JSON.stringify({
        
            id : this.state.TextInput_Nasabah_ID
        
          })
        
          }).then((response) => response.json())
          .then((responseJson) => {
        
            // Showing response message coming from server after inserting records.
            Alert.alert(responseJson);
        
          }).catch((error) => {
             console.error(error);
          });

          this.props.navigation.navigate('First');

      }

    render() {

      return (
   
   <View style={styles.MainContainer}>
   
          <Text style={{fontSize: 20, textAlign: 'center', marginBottom: 7}}> Edit Nasabah Record Form </Text>
    
          <TextInput
            
            placeholder="Nasabah Name Shows Here"
            
            value={this.state.TextInput_Nasabah_Name}
   
            onChangeText={ TextInputValue => this.setState({ TextInput_Nasabah_Name : TextInputValue }) }
   
            underlineColorAndroid='transparent'
   
            style={styles.TextInputStyleClass}
          />
   
         <TextInput
            
            placeholder="Nasabah Class Shows Here"

            value={this.state.TextInput_Nasabah_Address}
   
            onChangeText={ TextInputValue => this.setState({ TextInput_Nasabah_Address : TextInputValue }) }
   
            underlineColorAndroid='transparent'
   
            style={styles.TextInputStyleClass}
          />
   
         <TextInput
            
            placeholder="Nasabah Phone Number Shows Here"

            value={this.state.TextInput_Nasabah_PhoneNumber}
   
            onChangeText={ TextInputValue => this.setState({ TextInput_Nasabah_PhoneNumber : TextInputValue }) }
   
            underlineColorAndroid='transparent'
   
            style={styles.TextInputStyleClass}
          />
   
          <TextInput
   
            placeholder="Nasabah Email Shows Here"

            value={this.state.TextInput_Nasabah_Email}
   
            onChangeText={ TextInputValue => this.setState({ TextInput_Nasabah_Email : TextInputValue }) }
   
            underlineColorAndroid='transparent'
   
            style={styles.TextInputStyleClass}
          />
   
         <TouchableOpacity activeOpacity = { .4 } style={styles.TouchableOpacityStyle} onPress={this.UpdateNasabahRecord} >
   
            <Text style={styles.TextStyle}> UPDATE STUDENT RECORD </Text>
   
         </TouchableOpacity>
   
         <TouchableOpacity activeOpacity = { .4 } style={styles.TouchableOpacityStyle} onPress={this.DeleteNasabahRecord} >
   
            <Text style={styles.TextStyle}> DELETE CURRENT RECORD </Text>
   
         </TouchableOpacity>
    
   
   </View>
              
      );
    }

}

const MyNewProject = createStackNavigator(

  {

    First: { screen: MainActivity },

    Second: { screen: ShowNasabahListActivity },

    Third: { screen: EditNasabahRecordActivity }

  });

// export default MyNewProject = createStackNavigator(

//   {

//     First: { screen: MainActivity },

//     Second: { screen: ShowNasabahListActivity },

//     Third: { screen: EditNasabahRecordActivity }

//   });

const AppContainer = createAppContainer(MyNewProject);

export default class App extends React.Component {
	render() {
		return (
			<Provider store={store}>
				<AppContainer />
			</Provider>
		);
	}
}

const styles = StyleSheet.create({

  MainContainer :{

    alignItems: 'center',
    flex:1,
    paddingTop: 30,
    backgroundColor: '#fff'

  },

  MainContainer_For_Show_NasabahList_Activity :{
    
    flex:1,
    paddingTop: (Platform.OS == 'ios') ? 20 : 0,
    marginLeft: 5,
    marginRight: 5
    
    },

  TextInputStyleClass: {

  textAlign: 'center',
  width: '90%',
  marginBottom: 7,
  height: 40,
  borderWidth: 1,
  borderColor: '#FF5722',
  borderRadius: 5 ,

  },

  TouchableOpacityStyle: {

    paddingTop:10,
    paddingBottom:10,
    borderRadius:5,
    marginBottom:7,
    width: '90%',
    backgroundColor: '#00BCD4'

  },

  TextStyle:{
    color:'#fff',
    textAlign:'center',
  },

  rowViewContainer: {
    fontSize: 20,
    paddingRight: 10,
    paddingTop: 10,
    paddingBottom: 10,
  }

});