import { createStore, combineReducers } from 'redux';
import { email } from './reducers/login.reducer';

export const store = createStore(combineReducers({
    email: email,
}));