export function email(state, action) {
    if (typeof state === 'undefined') {
        return '';
    }

    switch (action.type) {
        case 'EMAIL':
            return action.value;
        default:
            return state;
    }
}